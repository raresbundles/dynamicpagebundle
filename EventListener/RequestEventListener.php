<?php

namespace Rares\DynamicPageBundle\EventListener;

use Rares\DynamicPageBundle\Helper\RequestHelper;
use Rares\DynamicPageBundle\Service\RequestService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestEventListener
{
    private $service;
    private $alwaysEnabled;
    private $redirectRoute;
    private $enableListener;

    /**
     * Create a new request event listener.
     *
     * @param RequestHelper $service
     * @param array $params
     */
    public function __construct(RequestService $service, $params)
    {
        $this->service = $service;
        $this->alwaysEnabled = $params['always_enabled'];
        $this->redirectRoute = $params['redirect_route'];
        $this->enableListener = $params['enable_listener'];
    }

    /**
     * If ajax calls are always enabled and this listener is enabled, redirect
     * all non ajax request (requests made in the browser) except
     * the requests for the redirect route to the specified route.
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->alwaysEnabled || !$this->enableListener
            || $this->service->ignoreIfDebug() || $event->isPropagationStopped()
        ) {
            return;
        }

        $response = $this->service->redirectNonAjax($this->redirectRoute);

        if ($response) {
            $event->setResponse($response);
        }
    }
}
