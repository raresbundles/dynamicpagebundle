<?php

namespace Rares\DynamicPageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('rares_dynamic_page');

        $rootNode
            ->children()
                ->booleanNode('always_enabled')->defaultTrue()->end()
                ->scalarNode('menu_active_class')->defaultValue('active')->end()
                ->scalarNode('redirect_route')->isRequired()->end()
                ->booleanNode('enable_listener')->defaultTrue()->end()
            ->end();

        return $treeBuilder;
    }
}
