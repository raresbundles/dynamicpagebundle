<?php

namespace Rares\DynamicPageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConfigurationController extends Controller
{
    /**
     * Returns the main template from this bundle.Maps bundle configuration to
     * javascript variables and includes the bundles scripts.js file.
     */
    public function javascriptAction()
    {
        $params = $this->getParameter('raresDynamicPage');

        return $this->render('RaresDynamicPageBundle::main.html.twig', $params);
    }
}
