# Overview
The DynamicPageBundle is a bundle for Symfony 3/4 which provides functionality for loading parts of a page dynamically without reloading the hole page.Basically, this bundle provides custom response objects and does requests from links or forms using ajax and depending on the response object returned, a different action will be done on the DOM.

# Installation
To install this bundle, first you need to have jQuery installed.

Then you need to require this bundle:

```
    composer require "rares/dynamic-page-bundle"
```

Then enable the bundle in your AppKernel file:

```php
$bundles = [
	...,
	new Rares\DynamicPageBundle\RaresDynamicPageBundle(),
];
```

The configure the bundle in your config.yml file:

```yml
rares_dynamic_page:
    always_enabled:     true # if all the links and forms should be handled via ajax; you probably want this false
    menu_active_class:  'active' # the class which is used when using the update menu response
    redirect_route:     'main_page' # required, the route to which to redirect requests that are not ajax (made from browser)
    enable_listener:    true # enable the redirect listener which redirects all non ajax requests to the redirect route.Only enabled if the always_enabled option is also true.
```

You will then have to install the assets using the command:

```
bin/console assets:install --symlink
```

Then you need to include the scripts from this bundle into your template.To do this, you have to render a controller action, after the jQuery script was included.Also, if you use the ModalBundle, render this action after you include the script from that bundle.
After this is done, if the **always_enabled** option is true, all links and forms that appear on pages that extend that template will be submitted via Ajax.To instead redirect normally, add the class **do-redirect** to the link or form.If the option is false, you need to add the class **do-dynamic** to a link or form if you want them to be handled using this module.

```twig
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('bundles/raresmodal/js/scripts.js') }}"></script> {# if you also want to use the ModalBundle, include that javascript first #}

{{ render(controller('RaresDynamicPageBundle:Configuration:javascript')) }}
```

If you want to have a loading icon (throbber) when clicking on a link and ajax requests are made or if you want a full page loader to appear when submitting forms, you should also include the css file included in this module:

```twig
<link href="{{ asset('bundles/raresdynamicpage/css/ajax-progress.css') }}" rel="stylesheet">
```

You can disable the throbber or the loader by adding the classes **no-throbber** or **no-loader** on a link or form respectively.By adding the class **add-throbber** on a form you can make it use the throbber instead and by adding the class **add-loader** to a link you can make it use the loader instead.

The throbber structure is a div added after the clicked element with the classes **ajax-progress** and **ajax-progress-throbber** and also a dynamic class if the clicked element has an id, **ajax-progress-ELEMENT_ID**.Inside this div there is also another div with the class **throbber** that renders a small gif file.
The loader structure is a div added at the end of the body with the class **loader** and a dynamic class if the clicked elements has an id, **loader-ELEMENT_ID**.Inside this div there is another div with the class **loader-icon** that renders a gif file on the hole screen.

# Features
This bundle provides multiple response classes used to dynamically update a page and also a DOM update class that can be used if you want to update multiple elements on the page in the same response.

The logic of this module is the following:
- if you click on a link or submit a form, the request to that url will be made through ajax and the response should be one of the base response classes included in the module or a class that extends them
- based on the response class, a different action is taken after the request was made and the coresponding elements are updated

Selecting which element to update is done by the element id.

There are 5 different actions that this module provides: add element, remove element, update element, update link and update menu.

This bundle also provides integration with the Modal Bundle, but make sure that you include the javascript file from that bundle first in your template.

### Add Element
To add a new element dynamically to the DOM, you need to return an AddElementResponse, which takes 3 arguments and an optional one.

The first argument is the id of the element after or before you want to add the new element, the second is the new element id (this needs to be the same as the id of the wrapper in the html you add, so in your template you can have a div wrapper with the same value as this on it).The third argument is the html of the new element which needs to have a wrapper containing the same id given in the second argument.This can come from a Twig template by using the **renderView** function in a controller.The fourth argument is optional, it is a boolean indicating if the new element should be aded after or before the specified element, defaults to after.

### Remove Element
To remove an element from the DOM you need to return an RemoveElementReponse, which takes an element id as an argument and it will remove the element with that id from the DOM.

### Update Element
To update an existing element's html, you need to return an UpdateElementResponse which takes 2 arguments, the element id and the new html.All the html inside the element with the specified id will be overwritten.

### Update Link
You can also update just a link tag if you return an UpdateLinkResponse.It takes 2 arguments and an optional one.The first is the link id which should be placed on an anchor tag, the second is the new url of the link and the third optional one is the new text, which can be null if you want to keep the old text.

### Update Menu
You can update the menu active item using the UpdateMenuResponse which takes 2 arguments, first is the menu item id and the second is the menu id if you have multiple menus on the page.The second argument defaults to **main-menu**.The menu is updated like follows:
- the menu active class specified in the bundle configuration is removed from all the li tags in the menu
- the class is added to the li tag with the respective id in the specified menu

### Update Multiple Elements
If you want to update multiple elements on the page at once (which can happen quite often), you can return an UpdateDOMResponse, which takes just one argument, an array containing any of the other 5 classes mentioned or a class that extends them.The order in the array is the order the elements will be updated, keep this in mind when updating a lot of page elements.

# More Information
In this bundle you also find a **RequestService** which can be retrieved from the container.This service has a useful method, **redirectNonAjax** that takes as an argument a route and checks to see if the current request was made using ajax or not.If it was made from the browser, it the returns a new **RedirectResponse** to the route mentioned.This can be useful inside controller actions if the always active config parameter is false to redirect page reloads on routes that return reseponse classes from this bundle to a specified route.

There is also a kernel event listener in this bundle which redirects new requests that are non ajax automatically to the specified route in the configuration.The redirecting is only done if the always active parameter is true, the listener is enabled, the route is not a debug route (used for the Symfony toolbar) and if the event propagation was not stopped.
The listener has a priority of -255 if you want to instead override the redirect route for specific requests.

**Take a look at the [demo](https://bitbucket.org/raresbundles/dynamicpagebundle/branch/demo) branch to see an example project that uses this bundle and dynamically loads all the elements of a page, never reloading the hole page after the initial page load.**
