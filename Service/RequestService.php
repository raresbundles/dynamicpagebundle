<?php

namespace Rares\DynamicPageBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;

class RequestService
{
    private $request;
    private $router;

    /**
     * Create a new request service.
     *
     * @param RequestStack $requestStack
     * @param Router $router
     */
    public function __construct(RequestStack $requestStack, Router $router)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->router = $router;
    }

    /**
     * Return a redirect response to the specified route if the current
     * request was made from the browser or null if not or if the current
     * route is equals to the redirect route to avoid redirect loops.
     *
     * @param string $redirectRoute
     * @param array $params
     *
     * @return RedirectResponse|null
     */
    public function redirectNonAjax($redirectRoute, $params = [])
    {
        if ($redirectRoute == $this->request->attributes->get('_route')) {
            return;
        }

        if(!$this->request->isXmlHttpRequest()) {
            return new RedirectResponse($this->router->generate($redirectRoute, $params));
        }
    }

    /**
     * Return true if the current route is a debug route.
     *
     * @return boolean
     */
    public function ignoreIfDebug()
    {
        $route = $this->request->attributes->get('_route');

        if (strstr($route, '_profiler') || strstr($route, '_wdt')
            || strstr($route, '_twig')
        ) {
            return true;
        }

        return false;
    }
}
