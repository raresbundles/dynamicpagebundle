<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class AddElementResponse extends JsonResponse
{
    /**
     * Return this response if you want to add a new element to the page.
     *
     * @param string $element
     *   The element id where you want to add the new element.
     * @param string $newElement
     *   The new element id.
     * @param string $html
     *   The new element html.
     * @param bool $after
     *   If true, add after the element, if not before.
     */
    public function __construct($element, $newElement, $html, $after = true)
    {
        parent::__construct([
            'element' => $element,
            'newElement' => $newElement,
            'html' => $html,
            'type' => 'add',
            'after' => $after,
        ]);
    }
}
