<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class UpdateLinkResponse extends JsonResponse
{
    /**
     * Return this response if you want to update a link on the page.
     *
     * @param string $element
     *   The link id.
     * @param string $newUrl
     *   The new url.
     * @param string $newText
     *   The new text or null to keep the same one.
     */
    public function __construct($element, $newUrl, $newText = null)
    {
        parent::__construct([
            'element' => $element,
            'type' => 'link',
            'newUrl' => $newUrl,
            'newText' => $newText,
        ]);
    }
}
