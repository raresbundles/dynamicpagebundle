<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class UpdateElementResponse extends JsonResponse
{
    /**
     * Return this response if you want to update the html of an element.
     *
     * @param string $element
     *   The element id.
     * @param string $html
     *   The new element html.
     */
    public function __construct($element, $html)
    {
        parent::__construct([
            'element' => $element,
            'html' => $html,
            'type' => 'element',
        ]);
    }
}
