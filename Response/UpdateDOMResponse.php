<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class UpdateDOMResponse extends JsonResponse
{
    /**
     * Return this response if you want to update multiple elements on the page
     * at the same time.
     *
     * @param array $elements
     *   An array of other responses.
     */
    public function __construct($elements)
    {
        $newElements = [];

        foreach ($elements as $element) {
            $newElements[] = json_decode($element->getContent());
        }

        parent::__construct($newElements);
    }
}
