<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class RedirectResponse extends JsonResponse
{
    /**
     * Return this response if you want to redirect the browser from a
     * dynamic action.
     *
     * @param string $url
     *   The url to redirect to.
     */
    public function __construct($url)
    {
        parent::__construct([
            'url' => $url,
            'type' => 'redirect',
        ]);
    }
}