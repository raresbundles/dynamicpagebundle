<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class UpdateMenuResponse extends JsonResponse
{
    /**
     * Return this response if you want to update the menu active link.
     * It will add the class specified in the bundle configuration to the
     * specific menu item.
     *
     * @param string $menuItem
     *   The menu item id. (the item should be a li tag).
     * @param string $menu
     *   The menu id.Default to 'menu-main'. (the menu should be an ul tag)
     */
    public function __construct($menuItem = null, $menu = 'menu-main')
    {
        parent::__construct([
            'type' => 'menu',
            'menuItem' => $menuItem,
            'menu' => $menu,
        ]);
    }
}
