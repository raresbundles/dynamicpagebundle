<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class CloseModalResponse extends JsonResponse
{
    /**
     * Return this response if you want to close a modal that is already open by its id.
     * Useful when having confirmation modals inside another modal.
     *
     * @param string $modalId
     */
    public function __construct($modalId = 'base-modal')
    {
        if (!class_exists('Rares\ModalBundle\Response\ModalOpenResponse')) {
            trigger_error('The Modal Bundle is not installed.');
        }

        parent::__construct([
            'type' => 'modal-close',
            'modalId' => $modalId,
        ]);
    }
}
