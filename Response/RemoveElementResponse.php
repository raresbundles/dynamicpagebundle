<?php

namespace Rares\DynamicPageBundle\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class RemoveElementResponse extends JsonResponse
{
    /**
     * Return this response if you want to remove an element from the page.
     *
     * @param string $element
     *   The element id.
     */
    public function __construct($element)
    {
        parent::__construct([
            'element' => $element,
            'type' => 'remove',
        ]);
    }
}
