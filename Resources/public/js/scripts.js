var dynamicPageFunctions = {};

/**
 * Add a progress throbber after the element when an ajax request is made.
 */
dynamicPageFunctions.addProgressThrobber = function (elem) {
    var divClass = '';

    if (elem.attr('id')) {
        divClass = ' ajax-progress-' + elem.attr('id');
    }

    var div = $("<div>", {"class": "ajax-progress ajax-progress-throbber" + divClass});
    var throbber = $("<div>", {"class": "throbber"});
    div.append(throbber);
    elem.after(div);

    return div;
};

/**
 * Add a full page loader when an ajax request is made.
 */
dynamicPageFunctions.addProgressLoader = function (elem) {
    var divClass = '';

    if (elem.attr('id')) {
        divClass = ' loader-' + elem.attr('id');
    }

    var div = $("<div>", {"class": "loader" + divClass});
    var throbber = $("<div>", {"class": "loader-icon"});
    div.append(throbber);
    $("body").append(div);

    return div;
};

/**
 * Handle update menu responses.
 */
dynamicPageFunctions.updateMenu = function(data) {
    var activeClass = dynamicPageParams.menuActiveClass;
    $('#' + data.menu + ' li').removeClass(activeClass);
    $('#' + data.menu + ' li#' + data.menuItem).addClass(activeClass);
};

/**
 * Handle update link responses.
 */
dynamicPageFunctions.updateLink = function(data) {
    var elem = $("#" + data.element);
    elem.attr('href', data.newUrl);

    if (data.newText) {
        elem.text(data.newText);
    }
};

/**
 * Handle update element responses.
 */
dynamicPageFunctions.updateElement = function(data) {
    $("#" + data.element).html(data.html);

    dynamicPageFunctions.modifyElements("#" + data.element + " ");
};

/**
 * Handle add element responses.
 */
dynamicPageFunctions.addElement = function(data) {
    if (data.after) {
        $("#" + data.element).after(data.html);
    } else {
        $("#" + data.element).before(data.html);
    }

    dynamicPageFunctions.modifyElements("#" + data.newElement + " ");
};

/**
 * Handle remove element responses.
 */
dynamicPageFunctions.removeElement = function(data) {
    $("#" + data.element).remove();
};

/**
 * Modify the DOM depending on the type of the response.
 */
dynamicPageFunctions.modifyDOM = function(data) {
    if (data.type === 'link') {
        dynamicPageFunctions.updateLink(data);
    } else if (data.type === 'element') {
        dynamicPageFunctions.updateElement(data);
    } else if (data.type === 'add') {
        dynamicPageFunctions.addElement(data);
    } else if (data.type === 'remove') {
        dynamicPageFunctions.removeElement(data);
    } else if (data.type === 'menu') {
        dynamicPageFunctions.updateMenu(data);
    } else if (data.type === 'modal-close') {
        $('#' + data.modalId).modal('hide');
    } else if (data.type === 'redirect') {
        window.location.href = data.url;
    } else if (data.modal && typeof modalFunctions !== 'undefined'
        && jQuery.isFunction(modalFunctions.handleOpenResponses)) {
        modalFunctions.handleOpenResponses(data);
    }
};

/**
 * Depending on the response type, modify the DOM accordingly.
 */
dynamicPageFunctions.handleResponse = function(data) {
    if (Array.isArray(data)) {
        data.forEach(dynamicPageFunctions.modifyDOM);
    } else {
        dynamicPageFunctions.modifyDOM(data);
    }
};

/**
 * Get the template at the specified url through ajax and update the page.
 */
dynamicPageFunctions.getTemplateAjax = function(url, throbber, notUseHistory) {
    $.ajax({
        url: url,
    }).done(function(data) {
        if (throbber) {
            throbber.remove();
        }

        if (notUseHistory !== true) {
            window.history.pushState('', '', url);
        }
        dynamicPageFunctions.handleResponse(data);
    });
};

/**
 * Update the page dynamically if clicking on a link.
 */
dynamicPageFunctions.getTemplate = function(elem) {
    var throbber = null;

    if (!elem.hasClass("no-throbber")) {
        throbber = dynamicPageFunctions.addProgressThrobber(elem);
    }

    if (elem.hasClass("add-loader")) {
        throbber = dynamicPageFunctions.addProgressLoader(elem);
    }

    dynamicPageFunctions.getTemplateAjax(elem.attr('href'), throbber, elem.hasClass('no-history'));
};

/**
 * Submit the form through ajax and then update the page with the data from the
 * response.
 */
dynamicPageFunctions.getFormTemplate = function(formObject) {
    var form = $(formObject);
    var elem = form.find(':focus');

    var throbber = null;

    if (!form.hasClass("no-loader")) {
        throbber = dynamicPageFunctions.addProgressLoader(elem);
    }

    if (form.hasClass("add-throbber")) {
        throbber = dynamicPageFunctions.addProgressThrobber(elem);
    }

    var formData = new FormData(formObject);
    formData.append('submit', elem.attr('value'));

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        contentType: false,
        processData: false,
        cache: false,
    }).done(function(data) {
        if (throbber) {
            throbber.remove();
        }

        dynamicPageFunctions.handleResponse(data);
    });
};

/**
 * Bind the click handler to the links if dynamic page updating is enabled for
 * that link.
 */
dynamicPageFunctions.modifyLinks = function(baseElement) {
    if (dynamicPageParams.alwaysEnabled) {
        if (baseElement && $(baseElement.slice(0, -1) + ":not(.do-redirect)").is('a')) {
            $(baseElement).click(dynamicPageFunctions.clickLink);
        }

        $((baseElement || '') + "a:not(.do-redirect)").click(dynamicPageFunctions.clickLink);
    } else {
        if (baseElement && $(baseElement.slice(0, -1) + ".do-dynamic").is('a')) {
            $(baseElement).click(dynamicPageFunctions.clickLink);
        }

        $((baseElement || '') + "a.do-dynamic").click(dynamicPageFunctions.clickLink);
    }
};

/**
 * When a link is clicked, prevent the event and dynamically update the page.
 */
dynamicPageFunctions.clickLink = function(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    dynamicPageFunctions.getTemplate($(this));
};

/**
 * Bind the submit handler to the forms if dynamic page updating is enabled
 * for that link.
 */
dynamicPageFunctions.modifyForms = function(baseElement) {
    if (dynamicPageParams.alwaysEnabled) {
        if (baseElement && $(baseElement.slice(0, -1) + ":not(.do-redirect)").is('form')) {
            $(baseElement).submit(dynamicPageFunctions.submitForm);
        }

        $((baseElement || '') + "form:not(.do-redirect)").submit(dynamicPageFunctions.submitForm);
    } else {
        if (baseElement && $(baseElement.slice(0, -1) + ".do-dynamic").is('form')) {
            $(baseElement).submit(dynamicPageFunctions.submitForm);
        }

        $((baseElement || '') + "form.do-dynamic").submit(dynamicPageFunctions.submitForm);
    }
};

/**
 * When a form is submitted, prevent the event and dynamically update the page.
 */
dynamicPageFunctions.submitForm = function(event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    dynamicPageFunctions.getFormTemplate(this);
};

/*
 * Bind the handlers to the forms and links.baseElement can be null.
 * If baseElement is not null, the the binding will happen only for children
 * of that element.
 */
dynamicPageFunctions.modifyElements = function(baseElement) {
    // Provide integration with the Modal Bundle.
    // Re-bind the handlers after the page was updated for that specific element.
    if (baseElement && typeof modalFunctions !== 'undefined'
        && jQuery.isFunction(modalFunctions.modifyElements)) {
        modalFunctions.modifyElements(baseElement);
    }

    dynamicPageFunctions.modifyLinks(baseElement);
    dynamicPageFunctions.modifyForms(baseElement);
};

$(document).ready(function() {
    dynamicPageFunctions.modifyElements();
});
